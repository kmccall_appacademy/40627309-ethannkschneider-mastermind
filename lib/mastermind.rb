class Code
  attr_reader :pegs

  PEGS = {
    "r" => "red", "o" => "orange", "y" => "yellow",
    "g" => "green", "b" => "blue", "p" => "purple"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.keys.sample }
    self.new(pegs)
  end

  def self.parse(colors)
    input = colors.downcase.split("")
    raise "Bad input: colors not supported" if input.any? { |color| !PEGS.keys.include?(color) }
    self.new(input)
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(guess)
    matches = 0
    @pegs.each_with_index do |color, index|
      matches += 1 if guess[index] == color
    end
    matches
  end

  def near_matches(guess)
    counter_hash = Hash.new(0)
    @pegs.each { |peg| counter_hash[peg] += 1 }

    matches = 0
    guess.pegs.each_with_index do |peg, index|
      if @pegs.include?(peg) && counter_hash[peg] > 0 #&& @pegs[index] != peg
        matches += 1
        counter_hash[peg] -= 1
      end
    end

    matches - self.exact_matches(guess)
  end

  def ==(code)
    return false if !code.is_a?(Code)
    return false if code.pegs.map(&:downcase) != self.pegs.map(&:downcase)
    return true if code.pegs.map(&:downcase) == self.pegs.map(&:downcase)
  end

end

class Game
  attr_reader :secret_code

  PEGS = {
    "r" => "red", "o" => "orange", "y" => "yellow",
    "g" => "green", "b" => "blue", "p" => "purple"
  }

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @guesses_left = 10
    @game_won = false
  end

  def get_guess
    PEGS.each do |key, val|
      puts "Type #{key} for #{val}"
    end
    puts "Enter your guess (e.g. 'xxxx'): "
    guess = gets.chomp
    raise "You guessed too many pegs!" if guess.length > 4
    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact #{@secret_code.exact_matches(code)}"
    puts "near #{@secret_code.near_matches(code)}"
  end

  def play_round
    puts "You have #{@guesses_left} guesses left."
    user_guess = get_guess
    display_matches(user_guess)
    @game_won = true if @secret_code == user_guess
    @guesses_left -= 1
  end

  def play
    puts "Secret Code: #{@secret_code.pegss}"
    puts "The secret code has been selected. Now guess!"
    until @game_won || @guesses_left < 1
      play_round
    end
    puts "You lose!" if @guesses_left < 1 && !@game_won
    puts "You win!" if @game_won
  end
end


if __FILE__ == $PROGRAM_NAME
  Game.new().play
end
